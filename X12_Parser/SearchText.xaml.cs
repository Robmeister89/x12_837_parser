﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace X12_Parser
{
    /// <summary>
    /// Interaction logic for SearchText.xaml
    /// </summary>
    public partial class SearchText : UserControl
    {
        public static SearchText _SearchText = null;

        public SearchText()
        {
            _SearchText = this;
            InitializeComponent();
            SearchForText.Focus();
        }

        public string FocusedControl { get; set; }

        private void SearchForBtn_Click(object sender, RoutedEventArgs e)
        {
            Height = 110;
            MainWindow.FocusedControl = FocusedControl;
            MainWindow.SearchStr = SearchForText.Text;
            MainWindow._MainWindow.SearchString(SearchForText.Text);
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            var popup = FindParent<Popup>(sender as Button);
            if (popup != null)
                popup.IsOpen = false;
        }

        private static T FindParent<T>(DependencyObject dependencyObject) where T : DependencyObject
        {
            var parent = LogicalTreeHelper.GetParent(dependencyObject);
            if (parent == null) return null;
            var parentT = parent as T;
            return parentT ?? FindParent<T>(parent);
        }

        private void SearchForText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                SearchForBtn_Click(sender, e);
        }

        private void Window_IsMouseCaptureWithinChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Opacity = 1;
        }

        private void UserControl_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity = 0.3;
        }

        private void SearchAllBtn_Click(object sender, RoutedEventArgs e)
        {
            Height = 300;
            MainWindow.FocusedControl = FocusedControl;
            MainWindow.SearchStr = SearchForText.Text;
            MainWindow._MainWindow.SearchAll(SearchForText.Text);
        }

    }
}
