﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using FontAwesome5;

namespace X12_Parser
{
    /// <summary>
    /// Interaction logic for OpenFile.xaml
    /// </summary>
    public partial class OpenFile : Window
    {
        public OpenFile()
        {
            Icon = ImageAwesome.CreateImageSource(EFontAwesomeIcon.Solid_Hospital, Brushes.DodgerBlue);
            InitializeComponent();
        }

        public OpenFile(string FileType)
        {
            Icon = ImageAwesome.CreateImageSource(EFontAwesomeIcon.Solid_Hospital, Brushes.DodgerBlue);
            InitializeComponent();
            fileType = FileType;

            string dir = Properties.Settings.Default.OpenDir;
            if (dir != string.Empty)
                curDir = new DirectoryInfo(dir);
            else
                curDir = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
        }

        string fileType;
        private DirectoryInfo curDir;
        private DirectoryInfo[] Directories;
        string curDirText { get; set; }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            PopulateTreeView(DirectoryTree, curDir.FullName);
            curDirText = CurDirTxt.Text;
        }

        private void PopulateTreeView(TreeView tree, string CurFolder)
        {
            tree.Items.Clear();
            try
            {
                DirectoryInfo dir = new DirectoryInfo(CurFolder);
                Directories = dir.GetDirectories();
                curDir = dir;

                EdiDirectory ediDir = new EdiDirectory() { Name = curDir.Name, Path = curDir.FullName };
                foreach (var d in curDir.GetDirectories())
                {
                    var subDir = new EdiDirectory() { Name = d.Name, Path = d.FullName };
                    ediDir.Directories.Add(subDir);
                }
                tree.Items.Add(ediDir);
                CurDirTxt.Text = ediDir.Path;
                PopulateFilesBox(FilesBoxGrid, ediDir.Path, "*." + fileType);
            }
            catch
            {
                curDir = new DirectoryInfo(curDirText);
                PopulateTreeView(DirectoryTree, curDir.FullName);
            }
        }

        private void PopulateTreeViewParent(TreeView tree, string CurFolder)
        {
            tree.Items.Clear();
            try
            {
                DirectoryInfo dir = new DirectoryInfo(CurFolder);
                Directories = dir.GetDirectories();
                curDir = dir;

                EdiDirectory ediDir = new EdiDirectory() { Name = curDir.Parent.Name, Path = curDir.Parent.FullName };
                foreach (var d in curDir.Parent.GetDirectories())
                {
                    var subDir = new EdiDirectory() { Name = d.Name, Path = d.FullName };
                    ediDir.Directories.Add(subDir);
                }
                tree.Items.Add(ediDir);
                CurDirTxt.Text = ediDir.Path;
                PopulateFilesBox(FilesBoxGrid, ediDir.Path, "*." + fileType);
            }
            catch
            {
                curDir = new DirectoryInfo(curDirText);
                PopulateTreeView(DirectoryTree, curDir.FullName);
            }
        }

        private void PopulateFilesBox(DataGrid grid, string Folder, string FileType)
        {
            grid.ItemsSource = null;
            DirectoryInfo dir = new DirectoryInfo(Folder);
            List<FileInfo> Files = dir.GetFiles(FileType).ToList();
            List<EdiFile> EdiFiles = new List<EdiFile>();
            Files.ForEach(f =>
            {
                var edi = new EdiFile(f.FullName, f.Name, f.LastWriteTime.ToShortDateString(), f.Length);
                EdiFiles.Add(edi);
            });
            grid.ItemsSource = EdiFiles;
        }

        private void UpDir_Click(object sender, RoutedEventArgs e)
        {
            curDir = new DirectoryInfo(CurDirTxt.Text);
            if (curDir.Parent != null)
                PopulateTreeViewParent(DirectoryTree, curDir.FullName);
            else
                MessageBox.Show("Can't go up a directory.");
        }

        private void ExitScrn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MinScrn_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void ChooseFileBtn_Click(object sender, RoutedEventArgs e)
        {
            var file = FilesBoxGrid.SelectedItem as EdiFile;
            if (file != null)
            {
                MainWindow._filePath = file.Path;
                DialogResult = true;
                Close();
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Properties.Settings.Default.OpenDir = curDir.FullName;
            Properties.Settings.Default.Save();
        }

        private void myDocs_Click(object sender, RoutedEventArgs e)
        {
            curDir = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
            PopulateTreeView(DirectoryTree, curDir.FullName);
        }

        private void FilesBoxGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var file = (sender as DataGrid).SelectedItem as EdiFile;
            if (file != null)
                ChooseFileBtn_Click(sender, null);
        }

        private void DirectoryTree_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var node = (sender as TreeView).SelectedItem as EdiDirectory;
            if (node != null)
            {
                if (node != DirectoryTree.Items[0])
                    PopulateTreeView(DirectoryTree, node.Path);
                else
                    PopulateTreeViewParent(DirectoryTree, node.Path);
            }
        }

        private void GoToBtn_Click(object sender, RoutedEventArgs e)
        {
            curDir = new DirectoryInfo(CurDirTxt.Text);
            PopulateTreeView(DirectoryTree, curDir.FullName);
        }

    }
}
