﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace X12_Parser
{
    /// <summary>
    /// Interaction logic for ErrorList.xaml
    /// </summary>
    public partial class ErrorList : Window
    {
        public ErrorList()
        {
            InitializeComponent();
        }

        public ErrorList(List<Error> e)
        {
            InitializeComponent();
            Errors = e;
            ErrorsGrid.ItemsSource = Errors;
        }

        private List<Error> Errors { get; set; }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void MinScrn_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void ExitScrn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ErrorsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Error error = ErrorsGrid.SelectedItem as Error;
            MainWindow.ErrorRow = error.RowNumber;
            DialogResult = true;
        }


    }
}
