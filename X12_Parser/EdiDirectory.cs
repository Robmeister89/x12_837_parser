﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace X12_Parser
{
    public class EdiDirectory
    {
        public string Path { get; set; }
        public string Name { get; set; }
        public ObservableCollection<EdiDirectory> Directories { get; set; }

        public EdiDirectory()
        {
            Directories = new ObservableCollection<EdiDirectory>();
        }
    }
}
