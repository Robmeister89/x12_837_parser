﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace X12_Parser
{
    public class EdiFile
    {
        public string Path { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public string Size { get; set; }

        public EdiFile(string p, string n, string d, long s)
        {
            Path = p;
            Name = n;
            Date = d;
            Size = ConvertSize(s);
        }

        private string ConvertSize(long size)
        {
            if (size >= 1000000000)
                return Math.Round((double)size / 1000000000, 2).ToString("N1") + " GB";
            else if (size >= 1000000)
                return Math.Round((double)size / 1000000, 2).ToString("N1") + " MB";
            else
                return Math.Round((double)size / 1000, 2).ToString("N1") + " KB";
        }
    }
}
