﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace X12_Parser
{
    public class Claim
    {
        public string ClaimNumber { get; set; }
        public int RowNumber { get; set; }
        public Claim(string c, int r)
        {
            ClaimNumber = c;
            RowNumber = r;
        }
    }
}
