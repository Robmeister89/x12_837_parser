﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace X12_Parser
{
    /// <summary>
    /// Interaction logic for ClaimList.xaml
    /// </summary>
    public partial class ClaimList : Window
    {
        public ClaimList()
        {
            InitializeComponent();
        }

        public ClaimList(List<Claim> c)
        {
            InitializeComponent();
            Claims = c;
            ClaimsGrid.ItemsSource = Claims;
        }

        private List<Claim> Claims { get; set; }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void MinScrn_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void ExitScrn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ClaimsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Claim claim = ClaimsGrid.SelectedItem as Claim;
            MainWindow.ClaimRow = claim.RowNumber;
            DialogResult = true;
        }

    }

}

