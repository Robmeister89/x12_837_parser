﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using ookii = Ookii.Dialogs.Wpf;
using Notification.Wpf;
using System.Runtime.CompilerServices;
using System.Threading;

namespace X12_Parser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static MainWindow _MainWindow = null;
        private readonly NotificationManager _notificationManager = new NotificationManager();

        public MainWindow()
        {
            InitializeComponent();
            _MainWindow = this;
            fileNameTxt.Text = "File Opened: N/A";
            prog.DoWork += DoBruteForceWork;
            prog.RunWorkerCompleted += OnBruteForceComplete;
            prog.ProgressBarStyle = ookii.ProgressBarStyle.MarqueeProgressBar;
        }



        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void MinScrn_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void ExitScrn_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.Save();
            Close();
        }


        private ookii.ProgressDialog prog = new ookii.ProgressDialog()
        {
            WindowTitle = "Parse Encounter Files",
            Text = "Parsing the file you've selected...",
            ShowTimeRemaining = true,
            ShowCancelButton = false
        };

        private void DoBruteForceWork(object sender, DoWorkEventArgs e)
        {
            Dispatcher.Invoke(() => ParseFile());
        }

        public static bool parsed = false;
        public static string _filePath = string.Empty;

        private void OnBruteForceComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            parsed = false;
            ShowNotification("Parsing Completed", "File parsing has completed!", NotificationType.Success, 5);
            //_filePath = string.Empty;
        }

        public void ParseBtn_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFile("txt").ShowDialog();
            if (ofd.HasValue && ofd.Value)
            {
                fileNameTxt.Text = $"File Opened: {_filePath}";
                prog.ShowDialog();
            }
        }

        double x1, x2;
        int tot_count, k;
        int max;

        public void ParseFile()
        {
            Claims = new List<Claim>();
            Errors = new List<Error>();

            SetupGrid.Children.Clear();
            SetupGrid.ColumnDefinitions.Clear();
            SetupGrid.RowDefinitions.Clear();

            string text = File.ReadAllText(_filePath);
            string[] rows = text.Split('~');
            int x = 0;
            int y = 0;
            int count = 0;
            max = 0;

            for (int i = 1; i == 2; i++)
            {
                ColumnDefinition col = new ColumnDefinition() { Width = new GridLength(75) };
                SetupGrid.ColumnDefinitions.Add(col);
            }

            while (y < rows.Length)
            {
                count = rows[y].Split('*').Count();
                if (count > max)
                    max = count;
                y += 1;
            }

            x1 = 100.0 / y;
            x2 = 100.0 / y;
            tot_count = y;
            k = 1;

            for (int i = 0; i < max; i++)
            {
                ColumnDefinition col0 = new ColumnDefinition() { Width = GridLength.Auto, MinWidth = 50.0 };
                SetupGrid.ColumnDefinitions.Add(col0);
            }

            while (x < rows.Length)
            {
                RowDefinition row0 = new RowDefinition() { Height = new GridLength(30) };
                SetupGrid.RowDefinitions.Add(row0);
                string[] cols = rows[x].Split('*');

                TextBlock txt0 = new TextBlock()
                {
                    Name = "Row" + x.ToString(),
                    Text = "Row " + (x + 1).ToString(),
                    TextAlignment = TextAlignment.Center,
                    Foreground = Brushes.DarkBlue,
                    Margin = new Thickness(2),
                    VerticalAlignment = VerticalAlignment.Center,
                };
                Grid.SetColumn(txt0, 0);
                Grid.SetRow(txt0, x);
                SetupGrid.Children.Add(txt0);

                Button btn0 = new Button()
                {
                    Content = "Delete",
                    Name = "Btn" + x.ToString(),
                    Margin = new Thickness(2),
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    VerticalContentAlignment = VerticalAlignment.Center
                };
                btn0.Click += (s, e) => DeleteBtn_Click(s, e);
                Grid.SetColumn(btn0, 1);
                Grid.SetRow(btn0, x);
                SetupGrid.Children.Add(btn0);

                Button btn1 = new Button()
                {
                    Content = "Insert",
                    Name = "BtnIn" + x.ToString(),
                    Margin = new Thickness(2),
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    ToolTip = "Insert a row above",
                };
                btn1.Click += (s, e) => InsertBtn_Click(s, e);
                Grid.SetColumn(btn1, 2);
                Grid.SetRow(btn1, x);
                SetupGrid.Children.Add(btn1);

                Button btn2 = new Button()
                {
                    Content = "Edit",
                    Name = "BtnEdit" + x.ToString(),
                    Margin = new Thickness(2),
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    ToolTip = "Edit the row",
                };
                btn2.Click += (s, e) => EditBtn_Click(s, e);
                Grid.SetColumn(btn2, 3);
                Grid.SetRow(btn2, x);
                SetupGrid.Children.Add(btn2);

                TextBlock txt = new TextBlock()
                {
                    Text = rows[x],
                    Name = "Txt" + x.ToString(),
                    Margin = new Thickness(2),
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Center,
                };

                if (cols[0] == "ST" || cols[0] == "SE")
                    txt.Foreground = Brushes.Blue;

                if (cols[0] == "CLM")
                {
                    var clm = new Claim(cols[1], x + 1);
                    Claims.Add(clm);
                    if (cols.Length == 12 && cols[11].StartsWith("AA"))
                    {
                        string[] clmCols = cols[11].Split(':');
                        if (clmCols.Length < 4)
                        {
                            txt.Foreground = Brushes.Red;
                            var error = new Error("Accident State Missing", x + 1, rows[x]);
                            Errors.Add(error);
                        }
                    }
                }

                if (cols[0] == "NM1" && NM1Check.Contains(cols[1]))
                {
                    if (cols.Length < 4)
                    {
                        txt.Foreground = Brushes.Red;
                        var error = new Error("NM1 Missing", x + 1, rows[x]);
                        Errors.Add(error);
                    }
                    else
                    {
                        if (cols.Length == 10)
                        {
                            if (cols[9].Length != 10)
                            {
                                txt.Foreground = Brushes.Red;
                                var error = new Error("Invalid NPI", x + 1, rows[x]);
                                Errors.Add(error);
                            }
                            else if (cols[9].Length == 10)
                            {
                                bool good = CheckNPI(cols[9]);
                                if (!good)
                                {
                                    // add to error list and color row red
                                    txt.Foreground = Brushes.Red;
                                    var error = new Error("Invalid NPI", x + 1, rows[x]);
                                    Errors.Add(error);
                                }
                            }

                            if (cols[2] == "1" && (cols[3] == string.Empty || (cols[3] != string.Empty && cols[4] == string.Empty)))
                            {
                                txt.Foreground = Brushes.Red;
                                var error = new Error("NM1 Indiv. Name Missing", x + 1, rows[x]);
                                Errors.Add(error);
                            }
                            else if (cols[2] == "2" && cols[3] == string.Empty)
                            {
                                txt.Foreground = Brushes.Red;
                                var error = new Error("NM1 Org. Name Missing", x + 1, rows[x]);
                                Errors.Add(error);
                            }
                        }
                        else
                        {
                            txt.Foreground = Brushes.Red;
                            var error = new Error("NM1 Segment Incomplete", x + 1, rows[x]);
                            Errors.Add(error);

                        }
                    }
                }

                if (cols[0] == "REF" && RefNpiCheck.Contains(cols[1]) && cols[2].Length != 9)
                {
                    // add to error list and color row red
                    txt.Foreground = Brushes.Red;
                    var error = new Error("Invalid EIN/SSN", x + 1, rows[x]);
                    Errors.Add(error);
                }

                if (cols[0] == "N4" && cols.Length < 4)
                {
                    txt.Foreground = Brushes.Red;
                    var error = new Error("Missing Zip", x + 1, rows[x]);
                    Errors.Add(error);
                }

                if (cols[0] == "N4" && cols.Length > 3 && string.IsNullOrWhiteSpace(cols[3]))
                {
                    txt.Foreground = Brushes.Red;
                    var error = new Error("Missing Zip", x + 1, rows[x]);
                    Errors.Add(error);
                }

                Grid.SetColumn(txt, 4);
                Grid.SetColumnSpan(txt, max);
                Grid.SetRow(txt, x);
                SetupGrid.Children.Add(txt);

                x += 1;

                prog.ReportProgress(Convert.ToInt32(x1));
                x1 += x2;
                Dispatcher.Invoke((Action)(() => prog.Description = "Parsing line " + k.ToString() + "/" + tot_count.ToString()));
                k++;
            }
            RowCount = SetupGrid.RowDefinitions.Count();
        }

        private static List<Claim> Claims { get; set; }

        private static List<Error> Errors { get; set; }

        private static List<string> NM1Check
        {
            get
            {
                List<string> checks = new List<string>()
                {
                    "71", "72", "77", "82", "85", "DN", "ZZ"
                };
                return checks;
            }
        }

        private static List<string> RefNpiCheck
        {
            get
            {
                List<string> Ref = new List<string>()
                {
                    "EI", "SY"
                };
                return Ref;
            }
        }

        private static bool CheckNPI(string npi)
        {
            int sum = 0;
            bool alternate = true;
            char[] nx = npi.ToArray();
            int last = int.Parse(nx[nx.Length - 1].ToString());
            for (int i = npi.Length - 2; i >= 0; i--)
            {
                int n = int.Parse(nx[i].ToString());
                if (alternate)
                {
                    n *= 2;
                    if (n > 9)
                    {
                        n = (n - 10) + 1;
                    }
                }
                sum += n;
                alternate = !alternate;
            }

            sum += 24;
            int next = (int)(Math.Ceiling((decimal)sum / 10) * 10);
            int check = next - sum;
            if (check == last)
                return true;
            else
                return false;
        }

        private void SaveRowClicked(object btn, RoutedEventArgs e)
        {
            Button saveBtn = SetupGrid.Children.OfType<Button>().Where(b => b.Name.Contains("BtnSave")).ToList().First();
            SaveRowBtn_Click(saveBtn, e);
            EditBtn_Click(btn, e);
        }

        private void EditRowClicked()
        {
            List<TextBox> boxes = SetupGrid.Children.OfType<TextBox>().ToList();
            if (boxes != null)
                boxes.First().Focus();
        }

        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            List<TextBox> currentBoxes = SetupGrid.Children.OfType<TextBox>().ToList();
            if (currentBoxes.Any())
            {
                _notificationManager.Show("Cannot Edit Multiple Rows!", "Please save the row before continuing.", areaName: "NotificationArea", expirationTime: TimeSpan.FromSeconds(7),
                    (o, args) => SaveRowClicked(btn, e), "Save Row",
                    (o, args) => EditRowClicked(), "Edit Row");
            }
            else
            {

                int row = Convert.ToInt32(btn.Name.Substring(7, btn.Name.Length - 7));
                string rowTxt = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == "Txt" + row.ToString()).FirstOrDefault().Text;
                oldRowText = rowTxt;
                string[] cols = rowTxt.Split('*');
                for (int i = 0; i < cols.Length; i++)
                {
                    TextBox txt = new TextBox()
                    {
                        Text = cols[i],
                        TextWrapping = TextWrapping.Wrap,
                        Name = "Txt_" + i.ToString(),
                        Margin = new Thickness(2),
                        VerticalContentAlignment = VerticalAlignment.Center
                    };
                    Grid.SetColumn(txt, i + 4);
                    Grid.SetRow(txt, row);
                    SetupGrid.Children.Add(txt);

                    if (i == cols.Length - 1)
                    {
                        txt.KeyDown += Txt_KeyDown;
                        newBoxName = i + 1;
                        newBoxCol = i + 5;
                        newBoxRow = row;
                    }
                }

                TextBox txt0 = SetupGrid.Children.OfType<TextBox>().Where(t => t.Name == "Txt_0").First();
                txt0.Focus();
                txt0.CaretIndex = txt0.Text.Length;


                SetupGrid.Children.Remove(btn);
                Button btn2 = new Button()
                {
                    Content = "Save",
                    Name = "BtnSave" + row.ToString(),
                    Margin = new Thickness(2),
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    ToolTip = "Save the row",
                };
                btn2.Click += (s, r) => SaveRowBtn_Click(s, r);
                Grid.SetColumn(btn2, 3);
                Grid.SetRow(btn2, row);
                SetupGrid.Children.Add(btn2);

                TextBlock txt_ = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == "Txt" + row.ToString()).FirstOrDefault();
                SetupGrid.Children.Remove(txt_);

            }
        }

        int newBoxName;
        int newBoxCol;
        int newBoxRow;

        private void Txt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                TextBox txt = new TextBox()
                {
                    Text = "",
                    TextWrapping = TextWrapping.Wrap,
                    Name = "Txt_" + newBoxName.ToString(),
                    Margin = new Thickness(2),
                    VerticalContentAlignment = VerticalAlignment.Center
                };
                Grid.SetColumn(txt, newBoxCol);
                Grid.SetRow(txt, newBoxRow);
                SetupGrid.Children.Add(txt);
                txt.KeyDown += Txt_KeyDown;
                newBoxName += 1;
                newBoxCol += 1;
            }
        }

        private string oldRowText = "";

        private void SaveRowBtn_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            int row = Convert.ToInt32(btn.Name.Substring(7, btn.Name.Length - 7));

            string newRow = string.Empty;
            TextBox txt_ = new TextBox();
            for (int i = 0; i < max; i++)
            {
                txt_ = SetupGrid.Children?.OfType<TextBox>()?.Where(t => t.Name == "Txt_" + i.ToString())?.FirstOrDefault();
                if (txt_ != null)
                {
                    newRow += txt_.Text + "*";
                    SetupGrid.Children.Remove(txt_);
                }
                else
                    break;
            }
            newRow = newRow.TrimEnd('*');

            TextBlock txt = new TextBlock()
            {
                Text = newRow,
                Name = "Txt" + row.ToString(),
                Margin = new Thickness(2),
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Center,
                Foreground = newRow.StartsWith("ST*") ? Brushes.Blue : newRow.StartsWith("SE*") ? Brushes.Blue : Brushes.Black
            };

            if (Errors.Any(x => x.RowNumber == row + 1))
            {
                if (newRow != oldRowText)
                {
                    var thisError = Errors.Where(x => x.RowNumber == row + 1).First();
                    Errors.Remove(thisError);
                }
                else
                    txt.Foreground = Brushes.Red;
            }

            oldRowText = "";

            Grid.SetColumn(txt, 4);
            Grid.SetColumnSpan(txt, max);
            Grid.SetRow(txt, row);
            SetupGrid.Children.Add(txt);

            SetupGrid.Children.Remove(btn);
            Button btn2 = new Button()
            {
                Content = "Edit",
                Name = "BtnEdit" + row.ToString(),
                Margin = new Thickness(2),
                HorizontalContentAlignment = HorizontalAlignment.Center,
                VerticalContentAlignment = VerticalAlignment.Center,
                ToolTip = "Edit the row",
            };
            btn2.Click += (s, r) => EditBtn_Click(s, r);
            Grid.SetColumn(btn2, 3);
            Grid.SetRow(btn2, row);
            SetupGrid.Children.Add(btn2);

        }


        public static int ColsInserted { get; set; }

        private void InsertBtn_Click(object sender, RoutedEventArgs e)
        {
            SetupGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(30) });

            Button btn = sender as Button;
            int row = Convert.ToInt32(btn.Name.Substring(5, btn.Name.Length - 5));
            bool exists = false;
            for (int j = 0; j < SetupGrid.RowDefinitions.Count(); j++)
            {
                TextBox txt_ = SetupGrid.Children.OfType<TextBox>().Where(t => t.Name.StartsWith("Txt_")).FirstOrDefault();
                if (txt_ != null)
                {
                    ShowNotification("Warning", "You cannot add a new row until all rows are saved.", NotificationType.Warning, 5);
                    exists = true;
                    break;
                }
            }
            if (!exists)
            {
                var insert = new InsertCols().ShowDialog();
                if (insert.HasValue && insert.Value)
                {
                    for (int i = SetupGrid.RowDefinitions.Count(); i-- > (row + 1);)
                    {
                        string _txtblockName = "Row" + (i - 1).ToString();
                        TextBlock _textBlock = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == _txtblockName).FirstOrDefault();
                        _textBlock.Name = "Row" + i.ToString();
                        _textBlock.Text = "Row " + (i + 1).ToString();
                        Grid.SetRow(_textBlock, i);
                        Grid.SetColumn(_textBlock, 0);

                        Button btn2 = SetupGrid.Children.OfType<Button>().Where(b => b.Name == "Btn" + (i - 1).ToString()).FirstOrDefault();
                        btn2.Name = "Btn" + i.ToString();
                        Grid.SetRow(btn2, i);
                        Grid.SetColumn(btn2, 1);

                        Button btn3 = SetupGrid.Children.OfType<Button>().Where(b => b.Name == "BtnIn" + (i - 1).ToString()).FirstOrDefault();
                        btn3.Name = "BtnIn" + i.ToString();
                        Grid.SetRow(btn3, i);
                        Grid.SetColumn(btn3, 2);

                        Button btn4 = SetupGrid.Children.OfType<Button>().Where(b => b.Name == "BtnEdit" + (i - 1).ToString()).FirstOrDefault();
                        btn4.Name = "BtnEdit" + i.ToString();
                        Grid.SetRow(btn4, i);
                        Grid.SetColumn(btn4, 3);

                        TextBlock txt = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == "Txt" + (i - 1).ToString()).FirstOrDefault();
                        txt.Name = "Txt" + i.ToString();
                        Grid.SetRow(txt, i);
                        Grid.SetColumn(txt, 4);
                        Grid.SetColumnSpan(txt, max);

                    }

                    TextBlock txt0 = new TextBlock()
                    {
                        Name = "Row" + row.ToString(),
                        Text = "Row " + (row + 1).ToString(),
                        TextAlignment = TextAlignment.Center,
                        Foreground = Brushes.DarkBlue,
                        Margin = new Thickness(2),
                        VerticalAlignment = VerticalAlignment.Center,
                    };
                    Grid.SetColumn(txt0, 0);
                    Grid.SetRow(txt0, row);
                    SetupGrid.Children.Add(txt0);

                    Button btn0 = new Button()
                    {
                        Content = "Delete",
                        Name = "Btn" + row.ToString(),
                        Margin = new Thickness(2),
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        VerticalContentAlignment = VerticalAlignment.Center
                    };
                    btn0.Click += (s, z) => DeleteBtn_Click(s, z);
                    Grid.SetColumn(btn0, 1);
                    Grid.SetRow(btn0, row);
                    SetupGrid.Children.Add(btn0);

                    Button btn1 = new Button()
                    {
                        Content = "Insert",
                        Name = "BtnIn" + row.ToString(),
                        Margin = new Thickness(2),
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        VerticalContentAlignment = VerticalAlignment.Center,
                        ToolTip = "Insert a row above",
                    };
                    btn1.Click += (s, z) => InsertBtn_Click(s, z);
                    Grid.SetColumn(btn1, 2);
                    Grid.SetRow(btn1, row);
                    SetupGrid.Children.Add(btn1);

                    Button btn00 = new Button()
                    {
                        Content = "Save",
                        Name = "BtnSave" + row.ToString(),
                        Margin = new Thickness(2),
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        VerticalContentAlignment = VerticalAlignment.Center,
                        ToolTip = "Save the row",
                    };
                    btn00.Click += (s, z) => SaveRowBtn_Click(s, z);
                    Grid.SetColumn(btn00, 3);
                    Grid.SetRow(btn00, row);
                    SetupGrid.Children.Add(btn00);

                    for (int i = 0; i < ColsInserted; i++)
                    {
                        TextBox txt = new TextBox()
                        {
                            TextWrapping = TextWrapping.Wrap,
                            Name = "Txt_" + i.ToString(),
                            Margin = new Thickness(2),
                            VerticalContentAlignment = VerticalAlignment.Center
                        };
                        Grid.SetColumn(txt, i + 4);
                        Grid.SetRow(txt, row);
                        SetupGrid.Children.Add(txt);
                    }

                    for (int i = row + 1; i < SetupGrid.RowDefinitions.Count(); i++)
                    {
                        string tblockName = "Txt" + i.ToString();
                        TextBlock _txt2 = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == tblockName).FirstOrDefault();
                        if (_txt2 != null && _txt2.Text.StartsWith("SE"))
                        {
                            string[] cols = _txt2.Text.Split('*');
                            string col2 = (Convert.ToInt32(cols[1]) + 1).ToString();
                            cols[1] = col2;
                            string newSE = string.Empty;
                            for (int j = 0; j < cols.Length; j++)
                                newSE += cols[j] + "*";
                            _txt2.Text = newSE.TrimEnd('*');
                            break;
                        }
                    }
                }
            }
        }

        // delete a row
        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            int row = Convert.ToInt32(btn.Name.Substring(3, btn.Name.Length - 3));
            TextBlock _txt = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == "Txt" + row.ToString()).FirstOrDefault();
            if (_txt.Text.StartsWith("ST*"))
            {
                while (true)
                {
                    DeleteRow(btn, row);
                    //row++;
                    btn = SetupGrid.Children.OfType<Button>().Where(t => t.Name == "Btn" + row.ToString()).FirstOrDefault();
                    _txt = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == "Txt" + row.ToString()).FirstOrDefault();
                    string txt = _txt.Text;
                    if (txt.StartsWith("ST*") || txt.StartsWith("GE"))
                        break;
                }

                for (int i = row + 1; i < SetupGrid.RowDefinitions.Count(); i++)
                {
                    string tblockName = "Txt" + i.ToString();
                    TextBlock _txt2 = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == tblockName).FirstOrDefault();
                    if (_txt2 != null && _txt2.Text.StartsWith("GE"))
                    {
                        string[] cols = _txt2.Text.Split('*');
                        string col2 = (Convert.ToInt32(cols[1]) - 1).ToString();
                        cols[1] = col2;
                        string newSE = string.Empty;
                        for (int j = 0; j < cols.Length; j++)
                            newSE += cols[j] + "*";
                        _txt2.Text = newSE.TrimEnd('*');
                        break;
                    }
                }
            }
            else
                DeleteRow(btn, row);
        }

        private void DeleteRow(Button btn, int row)
        {
            TextBlock row_ = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == "Row" + row.ToString()).FirstOrDefault();
            if (row_ != null)
                SetupGrid.Children.Remove(row_);
            if (btn != null)
                SetupGrid.Children.Remove(btn);

            TextBlock _txt = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == "Txt" + row.ToString()).FirstOrDefault();
            SetupGrid.Children.Remove(_txt);

            for (int i = row + 1; i < SetupGrid.RowDefinitions.Count(); i++)
            {
                string tblockName = "Txt" + i.ToString();
                TextBlock _txt2 = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == tblockName).FirstOrDefault();
                if (_txt2 != null && _txt2.Text.StartsWith("SE"))
                {
                    string[] cols = _txt2.Text.Split('*');
                    string col2 = (Convert.ToInt32(cols[1]) - 1).ToString();
                    cols[1] = col2;
                    string newSE = string.Empty;
                    for (int j = 0; j < cols.Length; j++)
                        newSE += cols[j] + "*";
                    _txt2.Text = newSE.TrimEnd('*');
                    break;
                }
            }

            for (int i = row + 1; i < SetupGrid.RowDefinitions.Count(); i++)
            {
                TextBlock _textBlock = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == "Row" + i.ToString()).FirstOrDefault();
                _textBlock.Name = "Row" + (i - 1).ToString();
                _textBlock.Text = "Row " + i.ToString();
                Grid.SetRow(_textBlock, i - 1);
                Grid.SetColumn(_textBlock, 0);

                Button btn2 = SetupGrid.Children.OfType<Button>().Where(b => b.Name == "Btn" + i.ToString()).FirstOrDefault();
                btn2.Name = "Btn" + (i - 1).ToString();
                Grid.SetRow(btn2, i - 1);
                Grid.SetColumn(btn2, 1);

                Button btn3 = SetupGrid.Children.OfType<Button>().Where(b => b.Name == "BtnIn" + i.ToString()).FirstOrDefault();
                btn3.Name = "BtnIn" + (i - 1).ToString();
                Grid.SetRow(btn3, i - 1);
                Grid.SetColumn(btn3, 2);

                Button btn4 = SetupGrid.Children.OfType<Button>().Where(b => b.Name == "BtnEdit" + i.ToString()).FirstOrDefault();
                btn4.Name = "BtnEdit" + (i - 1).ToString();
                Grid.SetRow(btn4, i - 1);
                Grid.SetColumn(btn4, 3);

                TextBlock txt = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == "Txt" + i.ToString()).FirstOrDefault();
                txt.Name = "Txt" + (i - 1).ToString();
                Grid.SetRow(txt, i - 1);
                Grid.SetColumn(txt, 4);
                Grid.SetColumnSpan(txt, max);
            }
            SetupGrid.RowDefinitions.RemoveAt(SetupGrid.RowDefinitions.Count() - 1);
        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            TextBox txt_ = SetupGrid.Children.OfType<TextBox>().Where(t => t.Name.StartsWith("Txt_")).FirstOrDefault();
            if (txt_ == null)
            {
                string _textFile = string.Empty;
                for (int i = 0; i < SetupGrid.RowDefinitions.Count() - 1; i++)
                {
                    string tblockName = "Txt" + i.ToString();
                    TextBlock txt = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == tblockName).FirstOrDefault();
                    if (txt != null)
                        _textFile += txt.Text + "~";
                    else
                        break;

                    if (_textFile[_textFile.Length - 2] == '~')
                        _textFile = _textFile.Substring(0, _textFile.Length - 1);
                }

                if (_textFile != string.Empty)
                {
                    _textFile.Substring(_textFile.Length - 1);
                    SaveFile(_textFile, _filePath);
                }
                else
                    ShowNotification("Nothing to Save", "Please parse a file first.", NotificationType.Warning, 5);
            }
            else
                ShowNotification("File Can't Be Saved!", "Please save all unsaved rows.", NotificationType.Error, 5);


        }

        // save the file without over-writing original
        private void SaveFile(string file, string filePath)
        {
            try
            {
                string date = DateTime.Now.ToString("yyyyMMdd");
                FileInfo curFile = new FileInfo(filePath);
                DirectoryInfo curDir = new DirectoryInfo(curFile.DirectoryName);
                string fileName = Path.GetFileNameWithoutExtension(curFile.FullName);

                string newFilePath = $@"{curDir.FullName}\{fileName}_{date}.txt";
                if (File.Exists(newFilePath))
                    File.Delete(newFilePath);

                using (var sw = new StreamWriter(newFilePath))
                {
                    sw.WriteLine(file);
                }

                ShowNotification("Saved!", "Your file has been saved.", NotificationType.Success, 3);
            }
            catch (Exception x)
            {
                ShowNotification("Danger!", "An error has occurred.", NotificationType.Error, 10);
                Logger.LogException(x);
            }
        }

        GoTo goTo;
        SearchText searchText;
        public static Popup pop;

        // keydown shortcuts
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox txt_ = SetupGrid.Children.OfType<TextBox>().Where(t => t.Name == "Txt_0").FirstOrDefault();

            if (e.Key == Key.O && Keyboard.IsKeyDown(Key.LeftCtrl))
                ParseBtn_Click(sender, null);

            if (e.Key == Key.G && Keyboard.IsKeyDown(Key.LeftCtrl))
            {
                if (txt_ == null)
                {
                    if (SetupGrid.RowDefinitions.Count() > 0)
                    {
                        goTo = new GoTo();
                        pop = _pop;
                        pop.Child = goTo;
                        pop.IsOpen = true;
                        pop.Placement = PlacementMode.Mouse;
                        pop.Focus();
                        GoTo._GoTo.GoToRow.Focus();
                    }
                }
                else
                    ShowNotification("Warning", "All rows must be saved to perform this action.", NotificationType.Warning, 7);
            }

            if (e.Key == Key.F && Keyboard.IsKeyDown(Key.LeftCtrl))
            {
                if (txt_ == null)
                {
                    if (SetupGrid.RowDefinitions.Count() > 0)
                    {
                        searchText = new SearchText() { Height = 110, };
                        if (FocusManager.GetFocusedElement(this) != null)
                            if (FocusManager.GetFocusedElement(this).GetType() == typeof(TextBox))
                                searchText.FocusedControl = (FocusManager.GetFocusedElement(this) as TextBox).Name;

                        pop = _pop;
                        pop.Child = searchText;
                        pop.IsOpen = true;
                        pop.Placement = PlacementMode.Mouse;
                        pop.Focus();
                        SearchText._SearchText.SearchForText.Focus();
                    }
                }
                else
                    ShowNotification("Warning", "All rows must be saved to perform this action.", NotificationType.Warning, 7);
            }

            if (e.Key == Key.S && Keyboard.IsKeyDown(Key.LeftCtrl))
                SaveBtn_Click(sender, null);

            if ((Keyboard.Modifiers & ModifierKeys.Alt) == ModifierKeys.Alt)
            {
                if (Keyboard.IsKeyDown(Key.Down))
                    ScrollViewer1.ScrollToBottom();
                if (Keyboard.IsKeyDown(Key.Up))
                    ScrollViewer1.ScrollToTop();
            }
        }

        private int RowCount { get; set; }
        public static string Row { get; set; }
        public static string SearchStr { get; set; }
        public static string FocusedControl { get; set; }

        // go to specific row
        public void GoToRow(int row)
        {
            Button btn = SetupGrid.Children.OfType<Button>().Where(b => b.Name == "BtnEdit" + row.ToString()).FirstOrDefault();
            TextBlock txt = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == "Txt" + row.ToString()).FirstOrDefault();
            txt.Focusable = true;
            var point = txt.TranslatePoint(new Point(), ScrollViewer1);
            ScrollViewer1.ScrollToVerticalOffset(point.Y - (txt.ActualHeight * 2));
        }

        // search for string based on input in pop up box
        public void SearchString(string str)
        {
            SearchText._SearchText.Height = 110;
            int row_;
            if (FocusedControl != null)
            {
                string name = FocusedControl;
                string row = name.Substring(7, name.Length - 7);
                row_ = Convert.ToInt32(row);
            }
            else
                row_ = 0;

            bool itemFound = false;
            for (int i = row_; i < RowCount - 2; i++)
            {
                string tblockName = "Txt" + i.ToString();
                TextBlock _textBlock = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == tblockName).FirstOrDefault();
                if (_textBlock != null)
                {
                    if (_textBlock.Text.Contains(str))
                    {
                        Button btn = SetupGrid.Children.OfType<Button>().Where(b => b.Name == "BtnEdit" + i.ToString()).FirstOrDefault();
                        btn.Focus();
                        searchText.FocusedControl = btn.Name;
                        itemFound = !itemFound;
                        return;
                    }
                }
                else
                    break;
            }

            if (!itemFound)
            {
                ScrollViewer1.ScrollToEnd();
                ShowNotification("Uh oh!", "You've reached the end of the document.", NotificationType.Information, 3);
            }
            itemFound = false;
        }

        // bring up search all box based on input from pop up box
        public void SearchAll(string str)
        {
            SearchText._SearchText.Height = 300;
            SearchText._SearchText.RichTextBox1.Document.Blocks.Clear();
            Paragraph para = new Paragraph();
            for (int i = 0; i < RowCount - 2; i++)
            {
                string tblockName = "Txt" + i.ToString();
                TextBlock _textBlock = SetupGrid.Children.OfType<TextBlock>().Where(t => t.Name == tblockName).FirstOrDefault();
                if (_textBlock != null)
                {
                    if (_textBlock.Text.Contains(str))
                    {
                        Button btn = SetupGrid.Children.OfType<Button>().Where(b => b.Name == "BtnEdit" + i.ToString()).FirstOrDefault();
                        btn.Focus();
                        searchText.FocusedControl = btn.Name;

                        Hyperlink link = new Hyperlink();
                        link.Inlines.Add("Row " + (i + 1).ToString() + ": " + _textBlock.Text);
                        link.Click += (s, e) => GoToRowString((link.Inlines.FirstInline as Run).Text);
                        para.Inlines.Add(link);
                        para.Inlines.Add(Environment.NewLine);
                    }
                }
                else
                    break;
            }
            SearchText._SearchText.RichTextBox1.Document.Blocks.Add(para);
        }

        // go to row method
        private void GoToRowString(string str)
        {
            string name = str;
            int _pos = name.IndexOf(":");

            int count = 0;
            for (int i = 3; i < _pos - 1; i++)
                count += 1;
            string row = name.Substring(4, count);

            int row_ = Convert.ToInt32(row) - 1;
            GoToRow(row_);
        }

        public static int ErrorRow { get; set; } = 0;

        private void ErrorsBtn_Click(object sender, RoutedEventArgs e)
        {
            var errors = new ErrorList(Errors).ShowDialog();
            if (errors.HasValue && errors.Value)
            {
                GoToRow(ErrorRow);
                Button editBtn = SetupGrid.Children.OfType<Button>().Where(b => b.Name == "BtnEdit" + (ErrorRow - 1).ToString()).FirstOrDefault();
                EditBtn_Click(editBtn, e);
                ErrorRow = 0;
            }
        }

        public static int ClaimRow { get; set; } = 0;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void ClaimsBtn_Click(object sender, RoutedEventArgs e)
        {
            var claims = new ClaimList(Claims).ShowDialog();
            if (claims.HasValue && claims.Value)
            {
                GoToRow(ClaimRow);
                ClaimRow = 0;
            }
        }

        private void ShowNotification(string title, string msg, NotificationType type, double time)
        {
            var content = new NotificationContent
            {
                Title = title,
                Message = msg,
                Type = type
            };
            _notificationManager.Show(content, areaName: "NotificationArea", expirationTime: TimeSpan.FromSeconds(time));
            Console.WriteLine($"{type}: {content.Message}");
        }


    }
}
