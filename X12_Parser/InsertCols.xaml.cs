﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace X12_Parser
{
    /// <summary>
    /// Interaction logic for InsertCols.xaml
    /// </summary>
    public partial class InsertCols : Window
    {
        public InsertCols()
        {
            InitializeComponent();
        }
        private void SearchForText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                SubmitBtn_Click(sender, e);
        }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            if (int.Parse(ColsInsert.Text).GetType().Equals(typeof(int)) && int.Parse(ColsInsert.Text) > 2)
            {
                MainWindow.ColsInserted = Convert.ToInt32(ColsInsert.Text);
                DialogResult = true;
            }
            else
                MessageBox.Show("Please enter a number (greater than 2) in the box.");
        }
    }
}
