﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace X12_Parser
{
    public class Error
    {
        public string ErrorType { get; set; }
        public int RowNumber { get; set; }
        public string Text { get; set; }

        public Error(string type, int row, string txt)
        {
            ErrorType = type;
            RowNumber = row;
            Text = txt;
        }
    }
}
