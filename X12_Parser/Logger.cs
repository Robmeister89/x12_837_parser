﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace X12_Parser
{
    class Logger
    {
        public static string filePath;

        public static void LogException(Exception x)
        {
            filePath = "ErrorLog.txt";
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("-----------------------------------------------------------------------------");
                writer.WriteLine("Date : " + DateTime.Now.ToString());
                writer.WriteLine();

                while (x != null)
                {
                    writer.WriteLine(x.GetType().FullName);
                    writer.WriteLine("Message : " + x.Message);
                    writer.WriteLine("StackTrace : " + x.StackTrace);
                    writer.WriteLine();
                    x = x.InnerException;
                }
            }
        }
    }
}
