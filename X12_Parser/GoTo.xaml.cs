﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace X12_Parser
{
    /// <summary>
    /// Interaction logic for GoTo.xaml
    /// </summary>
    public partial class GoTo : UserControl
    {
        public static GoTo _GoTo;

        public GoTo()
        {
            _GoTo = this;
            InitializeComponent();
            GoToRow.Focus();
        }

        private void GoToBtn_Click(object sender, RoutedEventArgs e)
        {
            MainWindow._MainWindow.GoToRow(Convert.ToInt32(GoToRow.Text) - 1);
        }

        private void GoToRow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                GoToBtn_Click(sender, e);
        }

        private static T FindParent<T>(DependencyObject dependencyObject) where T : DependencyObject
        {
            var parent = LogicalTreeHelper.GetParent(dependencyObject);
            if (parent == null) return null;
            var parentT = parent as T;
            return parentT ?? FindParent<T>(parent);
        }

        private void UserControl_LostFocus(object sender, RoutedEventArgs e)
        {
            Opacity = 0.3;
        }

        private void UserControl_IsMouseCaptureWithinChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Opacity = 1;
        }
    }
}
